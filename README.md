
<!-- README.md is generated from README.Rmd. Please edit that file -->

# treegrass

**Tree-Grass Bifurcation diagram** is a graphical tool developped by
researchers from CIRAD (in collaboration with researchers from AMAP-IRD
and the University of Pretoria, South Africa) to let ecologists easily
explore consequences of modifying parameter values in the system. In
particular, the impact of **fire frequency** and **mean annual
rainfall** are explored, showing all possible longterm behaviors of the
system, like Forest, Savannah, Grassland equilibrium, and their
combinations.

![An abrupt Forest–savanna (grassland) mosaic in Ayos,
Cameroon](man/figures/README-forest-savanna-mosaic.png)

It is a shiny application, developed under R, within the framework of
the paper “A minimalistic model of vegetation physiognomies in the
savanna biome”, authored by Valaire Yatat, Yves Dumont, Anna Doizy and
Pierre Couteron.

------------------------------------------------------------------------

-   Current version: 0.1.0
-   Developped at CIRAD by Yves Dumont and [Anna Doizy](doana-r.com)
-   Associated publication: [A minimalistic model of vegetation
    physiognomies in the savanna
    biome](https://doi.org/10.1016/j.ecolmodel.2020.109381)

## Local usage

You can install **treegrass** R package from
[gitlab](https://gitlab.com/cirad-apps/treegrass) with:

``` r
library(remotes)
install_gitlab("cirad-apps/tree-grass@0.1.0") # last release
# or
install_gitlab("cirad-apps/tree-grass") # development version
```

Run this command to launch the app locally:

``` r
library(treegrass)
run_app()
```

## Legend

The legend containing all of the 17 cases is presented below:

<img src="man/figures/README-legend-1.png" width="70%" />

#' Bifurcation diagram
#' where the bifurcation diagram is built
#' 
#' This function relies on the theoretical results obtained for each equilibrium in the manuscript. 
#' All cases summarized in Tables 1 and 2, page 10, are studied in this function. 
#' Only the roots of equation (B.5), page 37, are numerically estimated.  
#' Otherwise, the Local Asymptotic Analysis (LAS) follows the theorems proved in the manuscript.
#'
#' @param W mean annual rainfall
#' @param f fire frequency
#' @param parms named double with the model parameters
#'
#' @return the case number corresponding to the inputs
#' @export
#'
bif_computation<-function(W,f,parms){
#	
# Get distinct parameters from parms
names(parms) <- gsub("_parm", "", names(parms))
for (i in names(parms)) assign(i, parms[[i]])
	
#
	
coef <- 1
theta <- 2 # pas toucher
#
# Facilitation - Competition function related to W
#
etaTG_W <- function(W) {
	y<-a*tanh((W-b)/c)+d#0.0055*tanh((W-600)/100)+0.0045
}
#
# Fire intensity function
#
omegaG <- function(x,alpha){
  y<-(x)^(theta)/((x)^(theta)+(alpha)^(theta))#(x)^2/((x)^2+(alpha)^2)
}
#
# derivative of the Fire intensity function
#
omegaGder <- function(x,alpha){
  y<-theta*x^(theta-1)*alpha^(theta)/((x)^(theta)+(alpha)^(theta))^2#2*x*alpha^2/((x)^2+(alpha)^2)^2
}
#
# fire-induced grass mortality function
#
lambda_fG <- function (lambdafG_min,lambdafG_max,z,S){
  y<-lambdafG_min+(lambdafG_max-lambdafG_min)*W^z/(W^z+S^z)
}
#
# Fire-induced tree/shrub mortality function
#
Zeta <- function(T){
  y<-lambdafT_min+(lambdafT_max-lambdafT_min)*exp(-p*T)
}
#
# Derivative of the fire-induced tree/shrub mortality function
#
Zetader <- function(T){
  y<--p*(lambdafT_max-lambdafT_min)*exp(-p*T)
}
#
# definition of equation (B.5), page 37
H <- function(x){ 
  y<-cc*x^3-lambda*(x)^2*exp(alpha0*x)+(aa-bb-dd)*x^2+cc*(alpha)^2*x+(aa-bb)*(alpha)^2
}
#
# Threshold parameters functions related to savanna equilibrium asymptotic stability
#
R1_etoile <- function(x,xx) {
  A<-gammaGW/KG*x+gammaTW/KT*xx
  y<--f*omegaG(x,alpha)*xx*Zetader(xx)/A
}
R2_etoile <- function(x,xx) {
  A<--f*gammaGW/KG*omegaG(x,alpha)*Zetader(xx)+f*etaTG*omegaGder(x,alpha)*Zeta(xx)
  y<-gammaGW*gammaTW/(KG*KT)/A}
#
Q2_etoile <- function(x,xx) {
	omegaGetoile<-x^2/(x^2+alpha^2)
	der_omegaGetoile<-(2*alpha^2*x)/((x^2+alpha^2)^2)
	thetaTetoile<-lambdafT_min+(lambdafT_max-lambdafT_min)*exp(-p*xx)
	der_thetaTetoile<--p*(lambdafT_max-lambdafT_min)*exp(-p*xx)
	y<-((gammaGW*gammaTW)/(KG*KT)-f*etaTG*thetaTetoile*der_omegaGetoile)/(-f*gammaGW*omegaGetoile*der_thetaTetoile/KG)
}
#
    e0<-0; # Desert equilibrium stability state
    eT<-0; # Forest equilibrium stability state
    eG<-0; # Grassland equilibrium stability state
    eTG<-0; # Savanna equilibrium stability state
    eTG_2_1<-0; # Savanna equilibrium stability state when 2 equilibra exists
    eTG_2_2<-0; # Savanna equilibria stability states when 2 equilibra exists
    eTG_3_1<-0; # Savanna equilibrium stability state when 3 equilibra exists
    eTG_3_2<-0; # Savanna equilibria stability states when 3 equilibra exists
    eTG_3_3<-0; # Savanna equilibria stability states when 3 equilibra exists
    eTG_4_1<-0; # Savanna equilibrium stability state when 4 equilibra exists
    eTG_4_2<-0; # Savanna equilibria stability states when 4 equilibra exists
    eTG_4_3<-0; # Savanna equilibria stability states when 4 equilibra exists
    eTG_4_4<-0; # Savanna equilibria stability states when 4 equilibra exists
    eTG_5_1<-0 # Savanna equilibrium stability state when 5 equilibra exists
    eTG_5_2<-0 # Savanna equilibria stability states when 5 equilibra exists
    eTG_5_3<-0 # Savanna equilibria stability states when 5 equilibra exists
    eTG_5_4<-0 # Savanna equilibria stability states when 5 equilibra exists
    eTG_5_5<-0 # Savanna equilibria stability states when 5 equilibra exists
#
#
# Estimate of the model's parameters according to W and f
#
etaTG<-etaTG_W(W)
lambdafG<-lambda_fG(lambdafG_min,lambdafG_max,z,S)
gammaGW<-gammaG*(W)/(coef*bG+(W))
gammaTW<-gammaT*(W)/(coef*bT+(W))
KG<-cG/(1+dG*exp(-aG*W))
KT<-cT/(1+dT*exp(-aT*W))
#
QG<-gammaGW-deltaG
QT<-gammaTW-deltaT

## Estimate of some ecological thresholds, equilibria and periodic equilibria
#
R1W<-gammaTW/deltaT  
R2W<-gammaGW/(deltaG+f*lambdafG)
#
Gprairie<-(1-1/R2W)*KG # Grassland equilibrium
Tforet<-KT*(1-1/R1W) # Forest equilibrium
#
# Threshold parameter related to Grassland equilibrium
RG<-gammaTW/(deltaT+lambdafT_max*f*omegaG(Gprairie,alpha)) 
#
# Stability studies
#
if ((R1W<=1)& (R2W<=1)) { # Desert equilibrium is LAS if R1W<1 and R2W<1
  e0<-1 }
#
if ((R2W>1)&(RG<=1)) { # Grassland equilibrium is LAS if R2W<1 and RG<1
    eG<-1}
#
# The competition and neutral cases
if (etaTG>=0) {
	RF<-gammaGW/(etaTG*Tforet+deltaG+lambdafG*f) # Threshold related to the Forest equilibrium
	if ((R1W>1)&(RF<=1))  {     # Forest equilibrium is LAS if R1W>1 and RF<=1 
		eT<-1}
}
#
# Facilitation case
if (etaTG<0) {
	QF<-(gammaGW-etaTG*Tforet)/(deltaG+lambdafG*f) # Threshold related to the Forest equilibrium
	if ((R1W>1)&(QF<=1))  {    # Forest equilibrium is LAS if R1W>1 and QF<=1
		eT<-1}
}
#
# NO Fire case
if (f==0) {
	RF<-gammaGW/(etaTG*Tforet+deltaG+lambdafG*f)
		if ((R1W>1)&(R2W>1)&(RF>1)) {
eTG<-1
}}
#
# Characterization of Savanna existence and stability
#
# computation of the parameters defined in (B.1) page 36 in the manuscript
#
aa<-gammaTW*Tforet/KT
bb<-gammaGW*gammaTW*Gprairie/(etaTG*KG*KT)
cc<-bb/Gprairie
dd<-f*lambdafT_min
lambda<-f*(lambdafT_max-lambdafT_min)*exp(-p*gammaGW*Gprairie/(etaTG*KG))
alpha0<-p*gammaGW/(etaTG*KG)
#
# Based on Table 1, page 10
if (f>0) { # when fires occur
	if (etaTG>0) { # the competitive case
		if (R1W>1) { # avoit the Desert case
			if (R2W>1) {
				Se<-rootSolve::uniroot.all(H,c(0,Gprairie)) # find all roots of H(x)=0 in (0, Gprairie)
				if (length(Se)==1) { # only one positive root exists
					Te<-gammaGW*(Gprairie-Se)/(etaTG*KG) # estimate of the Tree Biomass in the Savanna equilibrium: see (B.3) page 36
					# estimate of the thresholds defined in (A.1), page 34
					R1<-R1_etoile(Se,Te)  
					R2<-R2_etoile(Se,Te)  
					if ((R1<1) & (R2>1)) { # the only savanna equilibrium is LAS if R1<1 and R2>1
						eTG<-1}}
				if (length(Se)==2) { # two positive roots exist
					Te<-gammaGW*(Gprairie-Se[1])/(etaTG*KG) # estimate of the Tree Biomass in the Savanna equilibrium: see (B.3) page 36
					# estimate of the thresholds defined in (A.1), page 34, for the first savanna equilibrium
					R1<-R1_etoile(Se[1],Te)  
					R2<-R2_etoile(Se[1],Te)  
					if ((R1<1) & (R2>1)) { # the first savanna equilibrium is LAS if R1<1 and R2>1
						eTG_2_1<-1}
					Te<-gammaGW*(Gprairie-Se[2])/(etaTG*KG) # estimate of the Tree Biomass in the Savanna equilibrium: see (B.3) page 36
					# estimate of the thresholds defined in (A.1), page 34 for the second savanna equilibrium
					R1<-R1_etoile(Se[2],Te)  
					R2<-R2_etoile(Se[2],Te)  
					if ((R1<1) & (R2>1)) { # the second savanna equilibrium is LAS if R1<1 and R2>1
						eTG_2_2<-1}}
				if (length(Se)==3) { # three positive roots exist
					Te<-gammaGW*(Gprairie-Se[1])/(etaTG*KG) # estimate of the Tree Biomass in the Savanna equilibrium: see (B.3) page 36
					# estimate of the thresholds defined in (A.1), page 34, for the first savanna equilibrium
					R1<-R1_etoile(Se[1],Te)  
					R2<-R2_etoile(Se[1],Te)  
					if ((R1<1) & (R2>1)) { # the first savanna equilibrium is LAS if R1<1 and R2>1
						eTG_3_1<-1}
					Te<-gammaGW*(Gprairie-Se[2])/(etaTG*KG) # estimate of the Tree Biomass in the Savanna equilibrium: see (B.3) page 36
					# estimate of the thresholds defined in (A.1), page 34, for the second savanna equilibrium
					R1<-R1_etoile(Se[2],Te)  
					R2<-R2_etoile(Se[2],Te)  
					if ((R1<1) & (R2>1)) { # the second savanna equilibrium is LAS if R1<1 and R2>1
						eTG_3_2<-1}
					Te<-gammaGW*(Gprairie-Se[3])/(etaTG*KG) # estimate of the Tree Biomass in the Savanna equilibrium: see (B.3) page 36
					# estimate of the thresholds defined in (A.1), page 34, for the third savanna equilibrium
					R1<-R1_etoile(Se[3],Te)  
					R2<-R2_etoile(Se[3],Te)  
					if ((R1<1) & (R2>1)) { # the third savanna equilibrium is LAS if R1<1 and R2>1
						eTG_3_3<-1}}
				if (length(Se)==4) { # four positive  roots exist
					Te<-gammaGW*(Gprairie-Se[1])/(etaTG*KG) # estimate of the Tree Biomass in the Savanna equilibrium: see (B.3) page 36
					# estimate of the thresholds defined in (A.1), page 34 for the first savanna equilibrium
					R1<-R1_etoile(Se[1],Te)  
					R2<-R2_etoile(Se[1],Te)  
					if ((R1<1) & (R2>1)) { # the first savanna equilibrium is LAS if R1<1 and R2>1
						eTG_4_1<-1}
					Te<-gammaGW*(Gprairie-Se[2])/(etaTG*KG) # estimate of the Tree Biomass in the Savanna equilibrium: see (B.3) page 36
					# estimate of the thresholds defined in (A.1), page 34, for the second savanna equilibrium
					R1<-R1_etoile(Se[2],Te)  
					R2<-R2_etoile(Se[2],Te)  
					if ((R1<1) & (R2>1)) { # the second savanna equilibrium is LAS if R1<1 and R2>1
						eTG_4_2<-1}
					Te<-gammaGW*(Gprairie-Se[3])/(etaTG*KG) # estimate of the Tree Biomass in the Savanna equilibrium: see (B.3) page 36
					# estimate of the thresholds defined in (A.1), page 34, for the third savanna equilibrium
					R1<-R1_etoile(Se[3],Te)  
					R2<-R2_etoile(Se[3],Te)  
					if ((R1<1) & (R2>1)) { # the third savanna equilibrium is LAS if R1<1 and R2>1
						eTG_4_3<-1}
					Te<-gammaGW*(Gprairie-Se[4])/(etaTG*KG) # estimate of the Tree Biomass in the Savanna equilibrium: see (B.3) page 36
					# estimate of the thresholds defined in (A.1), page 34, for the fourth savanna equilibrium
					R1<-R1_etoile(Se[4],Te)  
					R2<-R2_etoile(Se[4],Te)  
					if ((R1<1) & (R2>1)) { # the fourth savanna equilibrium is LAS if R1<1 and R2>1
						eTG_4_4<-1}}
				if (length(Se)==5) {  # five positive roots exist
					Te<-gammaGW*(Gprairie-Se[1])/(etaTG*KG) # estimate of the Tree Biomass in the first Savanna equilibrium: see (B.3) page 36
					# estimate of the thresholds defined in (A.1), page 34, for the first savanna equilibrium
					R1<-R1_etoile(Se[1],Te)  
					R2<-R2_etoile(Se[1],Te)  
					if ((R1<1) & (R2>1)) { # the first savanna equilibrium is LAS if R1<1 and R2>1
						eTG_5_1<-1}
					Te<-gammaGW*(Gprairie-Se[2])/(etaTG*KG) # estimate of the Tree Biomass in the second Savanna equilibrium: see (B.3) page 36
					# estimate of the thresholds defined in (A.1), page 34, for the second savanna equilibrium
					R1<-R1_etoile(Se[2],Te)  
					R2<-R2_etoile(Se[2],Te)  
					if ((R1<1) & (R2>1)) { # the second savanna equilibrium is LAS if R1<1 and R2>1
						eTG_5_2<-1}
					Te<-gammaGW*(Gprairie-Se[3])/(etaTG*KG) # estimate of the Tree Biomass in the third Savanna equilibrium: see (B.3) page 36
					# estimate of the thresholds defined in (A.1), page 34, for the third savanna equilibrium
					R1<-R1_etoile(Se[3],Te)  
					R2<-R2_etoile(Se[3],Te)  
					if ((R1<1) & (R2>1)) { # the third savanna equilibrium is LAS if R1<1 and R2>1
						eTG_5_3<-1}
					Te<-gammaGW*(Gprairie-Se[4])/(etaTG*KG) # estimate of the Tree Biomass in the fourth Savanna equilibrium: see (B.3) page 36
					# estimate of the thresholds defined in (A.1), page 34, for the fourth savanna equilibrium
					R1<-R1_etoile(Se[4],Te)  
					R2<-R2_etoile(Se[4],Te)  
					if ((R1<1) & (R2>1)) { # the fourth savanna equilibrium is LAS if R1<1 and R2>1
						eTG_5_4<-1}
					Te<-gammaGW*(Gprairie-Se[5])/(etaTG*KG) # estimate of the Tree Biomass in the fifth Savanna equilibrium: see (B.3) page 36
					# estimate of the thresholds defined in (A.1), page 34, for the fifth savanna equilibrium
					R1<-R1_etoile(Se[5],Te)  
					R2<-R2_etoile(Se[5],Te)  
					if ((R1<1) & (R2>1)) { # the fifth savanna equilibrium is LAS if R1<1 and R2>1
						eTG_5_5<-1}}
			}
		}
	}
  # end of the competition case
  #
	# NEUTRAL case etaTG=0
  # computations of parameters defined in formula (B.7), page 38
	omega<-omegaG(Gprairie,alpha)
	u<-KT*f*omega*lambdafT_min/gammaTW
	v<-KT*f*omega*(lambdafT_max-lambdafT_min)/gammaTW
	#
	if (etaTG==0){ # case 3, page 47
		if ((R1W>1)&(R2W>1)){
			if (-1+p*v>0){ # Debut I) 0, 1 or 2 savanna equilibria may exist (see page 38)
				T_1_etoile_bar<-(log(p*v))/p; # see page 47
				if (J(T_1_etoile_bar)>0){ 
					if (Tforet-u-v<0){ # subcase 2, page 48: at most 2 savanna equilibria exist
						Te1<-rootSolve::uniroot.all(J,c(0,T_1_etoile_bar)) # find the root in (0,\bar{T}_1^*) of the equation defined in formula (B.20)_2
						Te2<-rootSolve::uniroot.all(J,c(T_1_etoile_bar,10^6)) # find the root in (\bar{T}_1^*,+\infty) of the equation defined in formula (B.20)_2
						# thresholds estimates for each equilibrium
						if (length(Te1)>0) {
						  Ge1<-Gprairie # formula (B.20)_1
						  # estimate of the thresholds defined in (A.1), page 34
    					R1_1<-R1_etoile(Ge1,Te1)
		  				Q2_1<-Q2_etoile(Ge1,Te1)
			  			if ((Te1<Tforet)&(R1_1<1)&(Q2_1>1)){ # LAS of the first savanna equilibrium
				  		  eTG_2_1<-1}
					  }
						if (length(Te2)>0) { # if a root exists
						  Ge2<-Gprairie # formula (B.20)_1
						  # estimate of the thresholds defined in (A.1), page 34
  						R1_2<-R1_etoile(Ge2,Te2)
	  					Q2_2<-Q2_etoile(Ge2,Te2)
		  				if ((Te2<Tforet)&(R1_2<1)&(Q2_2>1)){ # LAS of the second savanna equilibrium
							eTG_2_2<-1}
						}
					}
					else{ #if Tforet-u-v>0 Sub-case 3, page 48. At most one savanna equilibrium exists
						Te<-rootSolve::uniroot.all(J,c(T_1_etoile_bar,10^6)) # find the root in (\bar{T}_1^*,+\infty) of the equation defined in formula (B.20)_2
						if (length(Te)>0) { # if a root exists
						  Ge<-Gprairie
						  # estimate of the thresholds defined in (A.1), page 34
						  R1<-R1_etoile(Ge,Te)
						  Q2<-Q2_etoile(Ge,Te)
						  if ((Te<Tforet)&(R1<1)&(Q2>1)){
							  eTG<-1}
						}
					}}} # Fin I)
			else{ #if -1+p*v<=0 - Case II, page 48
				if (Tforet-u-v>0){ # at most one savanna equilibrium exists
					Te<-rootSolve::uniroot.all(J,c(0,10^6)) # find the root in (0,+\infty) of the equation defined in formula (B.20)_2
					if (length(Te)>0) { # if a root exists
					  Ge<-Gprairie
					  # estimate of the thresholds defined in (A.1), page 34
					  R1<-R1_etoile(Ge,Te)
					  Q2=Q2_etoile(Ge,Te)
				  	if ((Te<Tforet)&(R1<1)&(Q2>1)){
					  	eTG<-1}
					}
				}} # Fin II)
		}}
	#
	#	
	# The Facilitation case
	#
	if (etaTG<0){
		if (R1W>1) {
			Se<-rootSolve::uniroot.all(H,c(Gprairie,KG)) # find all roots of H(x)=0 in (Gprairie, K_G)
			if (length(Se)==1) { # when one positive root exists then proceed
				Te<-gammaGW*(Gprairie-Se)/(etaTG*KG) # estimate of the Tree Biomass in the first Savanna equilibrium: see (B.3) page 36
				# estimate of the thresholds defined in (A.1), page 34, for the first savanna equilibrium
				R1<-R1_etoile(Se,Te)  
				Q2<-Q2_etoile(Se,Te)  
				if ((R1<1) & (Q2>1)) { # the first savanna equilibrium is LAS if R1<1 and Q2>1
					eTG<-1}}
			if (length(Se)==2) { # when two positive roots exist then proceed
				Te<-gammaGW*(Gprairie-Se[1])/(etaTG*KG) # estimate of the Tree Biomass in the first Savanna equilibrium: see (B.3) page 36
				# estimate of the thresholds defined in (A.1), page 34, for the first savanna equilibrium
				R1<-R1_etoile(Se[1],Te)  
				Q2<-Q2_etoile(Se[1],Te)  
				if ((R1<1) & (Q2>1)) { # the first savanna equilibrium is LAS if R1<1 and Q2>1
					eTG_2_1<-1}
				Te<-gammaGW*(Gprairie-Se[2])/(etaTG*KG) # estimate of the Tree Biomass in the second Savanna equilibrium: see (B.3) page 36
				# estimate of the thresholds defined in (A.1), page 34, for the second savanna equilibrium
				R1<-R1_etoile(Se[2],Te)  
				Q2<-Q2_etoile(Se[2],Te)  
				if ((R1<1) & (Q2>1)) { # the second savanna equilibrium is LAS if R1<1 and Q2>1
					eTG_2_2<-1}}
			if (length(Se)==3) { # when three positive roots exist then proceed
				Te<-gammaGW*(Gprairie-Se[1])/(etaTG*KG) # estimate of the Tree Biomass in the firstSavanna equilibrium: see (B.3) page 36
				# estimate of the thresholds defined in (A.1), page 34, for the first savanna equilibrium
				R1<-R1_etoile(Se[1],Te)  
				Q2<-Q2_etoile(Se[1],Te)  
				if ((R1<1) & (Q2>1)) { # the first savanna equilibrium is LAS if R1<1 and Q2>1
					eTG_3_1<-1}
				Te<-gammaGW*(Gprairie-Se[2])/(etaTG*KG) # estimate of the Tree Biomass in the Savanna equilibrium: see (B.3) page 36
				# estimate of the thresholds defined in (A.1), page 34, for the second savanna equilibrium
				R1<-R1_etoile(Se[2],Te)  
				Q2<-Q2_etoile(Se[2],Te)  
				if ((R1<1) & (Q2>1)) { # the second savanna equilibrium is LAS if R1<1 and Q2>1
					eTG_3_2<-1}
				Te<-gammaGW*(Gprairie-Se[3])/(etaTG*KG) # estimate of the Tree Biomass in the third Savanna equilibrium: see (B.3) page 36
				# estimate of the thresholds defined in (A.1), page 34, for the third savanna equilibrium
				R1<-R1_etoile(Se[3],Te)  
				Q2<-Q2_etoile(Se[3],Te)  
				if ((R1<1) & (Q2>1)) { # the third savanna equilibrium is LAS if R1<1 and Q2>1
					eTG_3_3<-1}}
			if (length(Se)==4) { # when four positive roots exist then proceed
				Te<-gammaGW*(Gprairie-Se[1])/(etaTG*KG) # estimate of the Tree Biomass in the first Savanna equilibrium: see (B.3) page 36
				# estimate of the thresholds defined in (A.1), page 34, for the first savanna equilibrium
				R1<-R1_etoile(Se[1],Te)  
				Q2<-Q2_etoile(Se[1],Te)  
				if ((R1<1) & (Q2>1)) { # the first savanna equilibrium is LAS if R1<1 and Q2>1
					eTG_4_1<-1}
				Te<-gammaGW*(Gprairie-Se[2])/(etaTG*KG) # estimate of the Tree Biomass in the second Savanna equilibrium: see (B.3) page 36
				# estimate of the thresholds defined in (A.1), page 34, for the second savanna equilibrium
				R1<-R1_etoile(Se[2],Te)  
				Q2<-Q2_etoile(Se[2],Te)  
				if ((R1<1) & (Q2>1)) { # the second savanna equilibrium is LAS if R1<1 and Q2>1
					eTG_4_2<-1}
				Te<-gammaGW*(Gprairie-Se[3])/(etaTG*KG) # estimate of the Tree Biomass in the third Savanna equilibrium: see (B.3) page 36
				# estimate of the thresholds defined in (A.1), page 34, for the third savanna equilibrium
				R1<-R1_etoile(Se[3],Te)  
				Q2<-Q2_etoile(Se[3],Te)  
				if ((R1<1) & (Q2>1)) { # the third savanna equilibrium is LAS if R1<1 and Q2>1
					eTG_4_3<-1}
				Te<-gammaGW*(Gprairie-Se[4])/(etaTG*KG) # estimate of the Tree Biomass in the fourth Savanna equilibrium: see (B.3) page 36
				# estimate of the thresholds defined in (A.1), page 34, for the fourth savanna equilibrium
				R1<-R1_etoile(Se[4],Te)  
				Q2<-Q2_etoile(Se[4],Te)  
				if ((R1<1) & (Q2>1)) {# the fourth savanna equilibrium is LAS if R1<1 and Q2>1
					eTG_4_4<-1}}
			if (length(Se)==5) { # when five positive roots exist then proceed
				Te<-gammaGW*(Gprairie-Se[1])/(etaTG*KG) # estimate of the Tree Biomass in the first Savanna equilibrium: see (B.3) page 36
				# estimate of the thresholds defined in (A.1), page 34, for the first savanna equilibrium
				R1<-R1_etoile(Se[1],Te)  
				Q2<-Q2_etoile(Se[1],Te)  
				if ((R1<1) & (Q2>1)) {  # the first savanna equilibrium is LAS if R1<1 and Q2>1
					eTG_4_1<-1}
				Te<-gammaGW*(Gprairie-Se[2])/(etaTG*KG) # estimate of the Tree Biomass in the second Savanna equilibrium: see (B.3) page 36
				# estimate of the thresholds defined in (A.1), page 34, for the second savanna equilibrium
				R1<-R1_etoile(Se[2],Te)  
				Q2<-Q2_etoile(Se[2],Te)  
				if ((R1<1) & (Q2>1)) {  # the second savanna equilibrium is LAS if R1<1 and Q2>1
					eTG_5_2<-1}
				Te<-gammaGW*(Gprairie-Se[3])/(etaTG*KG) # estimate of the Tree Biomass in the third Savanna equilibrium: see (B.3) page 36
				# estimate of the thresholds defined in (A.1), page 34, for the third savanna equilibrium
				R1<-R1_etoile(Se[3],Te)  
				Q2<-Q2_etoile(Se[3],Te)  
				if ((R1<1) & (Q2>1)) {  # the third savanna equilibrium is LAS if R1<1 and Q2>1
					eTG_5_3<-1}
				Te<-gammaGW*(Gprairie-Se[4])/(etaTG*KG) # estimate of the Tree Biomass in the fourth Savanna equilibrium: see (B.3) page 36
				# estimate of the thresholds defined in (A.1), page 34, for the fourth savanna equilibrium
				R1<-R1_etoile(Se[4],Te)  
				Q2<-Q2_etoile(Se[4],Te)  
				if ((R1<1) & (Q2>1)) {  # the fourth savanna equilibrium is LAS if R1<1 and Q2>1
					eTG_5_4<-1}
				Te<-gammaGW*(Gprairie-Se[5])/(etaTG*KG) # estimate of the Tree Biomass in the fifth Savanna equilibrium: see (B.3) page 36
				# estimate of the thresholds defined in (A.1), page 34, for the fifth savanna equilibrium
				R1<-R1_etoile(Se[5],Te)  
				Q2<-Q2_etoile(Se[5],Te)  
				if ((R1<1) & (Q2>1)) {  # the fifth savanna equilibrium is LAS if R1<1 and Q2>1
					eTG_5_5<-1}}
		}
	} # end of the case etaTG<0
} # end of f>0
#	    
# Bifurcation diagram 
# We count the total number of LAS of Tree-Grass equilibria
compteurTG<-eTG+eTG_2_1+eTG_2_2+eTG_3_1+eTG_3_2+eTG_3_3+eTG_4_1+eTG_4_2+eTG_4_3+
	eTG_4_4+eTG_5_1+eTG_5_2+eTG_5_3+eTG_5_4+eTG_5_5
#
# we determine the color code that will be used to represent all possible cases
output<-0
#
# We first assume that Forest and Grass equilibria are LAS
if ((eG==1)&(eT==1)) {
  
  if (compteurTG==3){ # Assume that 3 Tree-Grass equilibria are LAS
  	output<-16
  }
  if (compteurTG==2){ # Assume that 2 Tree-Grass equilibria are LAS
  	output<-13
  }
	if (compteurTG==1){ # Assume that 1 Tree-Grass equilibrium is LAS
		output<-8
	}
}
#

# bistable case Forest-Grassland
  if ((eG==1)&(eT==1)&(compteurTG==0)) {
  	output<-5
     }

# bistable case Grassland - Savanna
if ((eG==1)&(eT<1)) { # only the Grassland equilibrium is LAS

  if (compteurTG==2){ # Assume that 2 Tree-Grass equilibria are LAS
  	output<-10
  }
	if (compteurTG==1){ # Assume that 1 Tree-Grass equilibrium is LAS
		output<-7
	}
}

# bistable case Forest - Savanna

if ((eG<1)&(eT==1)) {

  if (compteurTG==3){ # Assume that 3 Tree-Grass equilibria are LAS
  	output<-14
  }
  if (compteurTG==2){ # Assume that 2 Tree-Grass equilibria are LAS
  	output<-9
  }
	if (compteurTG==1){ # Assume that 1 Tree-Grass equilibrium is LAS
		output<-6
	}
}

#
# monostability Grassland 
if ((eG==1)&(eT<1)&(compteurTG==0)) {
	output<-2
    }

# monostability Forest
if ((eG<1)&(eT==1)&(compteurTG==0)) {      
	output<-1
}

# Savanna only stability

if ((eG<1)&(eT<1)) {
  if (compteurTG==5){  # Assume that 5 Tree-Grass equilibria are LAS
  	output<-15
  }
  if (compteurTG==4){  # Assume that 4 Tree-Grass equilibria are LAS
  	output<-12
  }
  if (compteurTG==3){  # Assume that 3 Tree-Grass equilibria are LAS
  	output<-11
  }
  if (compteurTG==2){  # Assume that 2 Tree-Grass equilibria are LAS
  	output<-4
  }
	if (compteurTG==1){  # Assume that 1 Tree-Grass equilibria are LAS
		output<-3
	}
}
#
# Desert monostability 
if ((eTG==0)&(eT==0)&(eG==0)&(e0==1)) {
        output<-0
}
return(output)
# end of bif_computation
}

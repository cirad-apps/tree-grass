## code to prepare `DATASET` dataset goes here


# DATA --------------------------------------------------------------------


diagramLegend <- read.csv("inst/extdata/Legend.csv", fileEncoding = "UTF-8")

parameters <- read.csv("inst/extdata/Parameters.csv", encoding = "UTF-8", stringsAsFactors = F)
parameters$id <- paste(parameters$id, "parm", sep = "_") # so they can be detected  automatically in the server part



# DIAGRAM LEGEND ----------------------------------------------------------


case.colours <- c("#004506", "#0A8202", "#00B80F", "#3CFF2E", "#060080", "#0010F5", "#008FB3", "#00DDFF", "#A80707", "#FC0808", "#B07810", "#FFBB00", "#7A0382", "#BE00CC", "#FCA1FF", "#BBBBBB", "#000000")
names(case.colours) <- diagramLegend$Case


# specific colorScale for the heatmap (plot_ly)
zseq <- seq(0, 1, length.out = nrow(diagramLegend) + 1)
colorScale <- data.frame(
  z = c(0, rep(zseq[-c(1,length(zseq))],each = 2), 1)
  , col = rep(case.colours, each = 2)
  , stringsAsFactors = F
) 



usethis::use_data(diagramLegend, parameters, case.colours, colorScale, overwrite = TRUE)
